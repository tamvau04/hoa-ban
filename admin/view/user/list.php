<div>
	<h2 style="text-align: center; color: red; margin-bottom: 20px;">Quản lý tài khoản</h2>
	<h3 style="text-align: center;">Thêm tài khoản</h3>
	<a href="index.php?page=user&action=insert" style="margin-left: 420px;"><img src="../template/image/insert.png" width="70px" height="60px" style="border-radius:10px;"></a>
	<table border="1" width="80%" style="margin-left:50px;">
		<tr style="background:#7f827b; height: 50px; text-align: center; font-weight: bold;">
			<td width="15%">Tài khoản</td>
			<td>Mật khẩu</td>
			<td>Họ tên</td>
			<td>Ngày sinh</td>
			<td>Giới tính</td>
			<td>Quyền</td>
			<td width="8%">Sửa</td>
			<td width="7%">Xóa</td>
		</tr>
		<?php foreach ($_SESSION['list_User'] as $value) { ?>
		<tr>
			<td style="text-align: center; font-weight: bold;"><?php echo $value['username'];?></td>
			<td style="text-align: center; font-weight: bold;"><?php echo $value['password'];?></td>
			<td style="text-align: center;"><?php echo $value['fullname'];?></td>
			<td style="text-align: center;"><?php echo date("d-m-Y", strtotime($value['birthday']));?></td>
			<td style="text-align: center;"><?php echo $value['sex'] == 1 ? 'Nam' : 'Nữ' ;?></td>
			<td style="text-align: center; font-weight: bold;"><?php echo $value['name'];?></td>
			<td style="padding-left:5px;"><a href="index.php?page=user&action=update&id=<?php echo $value['id_User'];?>"><img src="../template/image/edit.jpg" width="50px" height="40px" style="border-radius:10px;"></a></td>
			<td><a href="index.php?page=user&action=delete&id=<?php echo $value['id_User'];?>"><img src="../template/image/delete.png" width="60px" height="50px"></a></td>
		</tr>
		<?php } ?>
	</table>
</div> 