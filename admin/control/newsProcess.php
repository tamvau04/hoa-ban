<?php
$news = $_SESSION['news'];

$title_article = $description = $images_1 = $images_2 = $detail_1 = $detail_2 = "";
if (isset($_POST['btOk_news'])) {
	$title_article = isset($_POST['txtitle_article']) && $_POST['txtitle_article'] != null ? $_POST['txtitle_article'] :'';
	$description = isset($_POST['txdescription']) && $_POST['txdescription'] != null ? $_POST['txdescription'] : '';

	$file_1 = $_FILES['img_1'];
	if ($file_1['name'] == null) {//nếu không chọn file ảnh mới thì sẽ giữ lại file ảnh  cũ
		$images_1 = $news['images_1'];
	}else{
		if ($file_1['size'] > 1048576) {
			echo "File không được lớn hơn 1MB";
		}else{
			$images_1 = $file_1['name'];
			move_uploaded_file($file_1['tmp_name'], "../template/image/".$images_1);
		}
	}
	
	$file_2 = $_FILES['img_2'];
	if ($file_2['name'] == null) {//nếu không chọn file ảnh mới thì sẽ giữ lại file ảnh  cũ
		$images_2 = $news['images_2'];
	}else{
		if ($file_2['size'] > 1048576) {
			echo "File không được lớn hơn 1MB";
		}else{
			$images_2 = $file_2['name'];
			move_uploaded_file($file_2['tmp_name'], "../template/image/".$images_2);
		}
	}

	$detail_1 = isset($_POST['txdetail_1']) && $_POST['txdetail_1'] != null ? $_POST['txdetail_1'] :'';
	$detail_2 = isset($_POST['txdetail_2']) && $_POST['txdetail_2'] != null ? $_POST['txdetail_2'] :'';
	if ($_POST['category'] == 0) {//nếu không chọn ô textbox thì nó sẽ giữ nguyên giá trị id cũ
		$id_News = $news['id_News'];
	}else{
		$id_News = $_POST['category'];
	}
	
	include_once("model/news.php");
	$news = new News();
	if (isset($_GET['id'])) {
		$news->update($_GET['id'],$title_article,$description,$images_1,$images_2, $detail_1, $detail_2,$id_News);
	}else{
		$news->insert($title_article,$description,$images_1,$images_2, $detail_1, $detail_2,$id_News);
	}
	header("location:index.php?page=news");
}
?>