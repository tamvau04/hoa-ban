<?php
include_once('model/product.php');
$p = new Products();
$_SESSION['list'] =  $p->getList();

if(isset($_GET['id']) && $_GET['id'] != null) {
	$id = $_GET['id'];
	$_SESSION['one'] = $p->getOne($id);
	include('view/product/one.php');
} else{
	if (!isset($_GET['p'])) {
		$pages = 1;
	}else{
		$pages = $_GET['p'];
	}
	$limit = 15;
	$_SESSION['totalPage'] = ceil(count($_SESSION['list'])/ $limit);
	$start = ($pages - 1) * $limit;
	$_SESSION['listPage'] = $p->getListPage($start, $limit);
	include('view/product/list.php');
}
?>