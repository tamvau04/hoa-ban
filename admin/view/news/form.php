<?php
 	$news = isset($_GET['id']) ? $_SESSION['news'] : '';
?>
<div>
	<h2 style="text-align: center; color: red; margin-bottom:20px;"><?php echo isset($_GET['id']) ? "Sửa bài viết" : "Thêm bài viết";?></h2>
	<form action="" method="post" enctype="multipart/form-data">
		<table style="margin-left: 150px;">
		<tr>
			<td width="20%" style="font-weight: bold;">Tên bài viết</td>
			<td style="padding: 7px 7px;"><input type="text" name="txtitle_article" value="<?php echo isset($_GET['id']) ? $news['title_article'] : '';?>" style="height: 30px; width: 300px;"></td>
		</tr>
		<tr>
			<td style="font-weight: bold;">Mô tả</td>
			<td style="padding: 7px 7px;"><textarea name="txdescription" cols="60" rows="13"><?php echo isset($_GET['id']) ? $news['description_article'] : '';?></textarea></td>
		</tr>
		<tr>
			<td style="font-weight: bold;">Chọn ảnh 1</td>
			<td style="padding: 7px 7px;"><input type="file" name="img_1"></td>
			<tr>
				<td></td>
				<td style="padding-left: 7px;">
					<?php if (isset($_GET['id'])) {?>
				   		<img src="../template/image/<?php echo $news['images_1'];?>" width="100px" height="70px" >
			   		<?php }?>
				</td>
			</tr>
		</tr>
		<tr>
			<td style="font-weight: bold;">Nội dung 1</td>
			<td style="padding: 7px 7px;"><textarea name="txdetail_1" cols="60" rows="13"><?php echo isset($_GET['id']) ? $news['detail_1'] : '';?></textarea></td>
		</tr>
		<tr>
			<td style="font-weight: bold;">Chọn ảnh 2</td>
			<td style="padding: 7px 7px;"><input type="file" name="img_2"></td>
			<tr>
				<td></td>
				<td style="padding-left: 7px;">
					<?php if (isset($_GET['id'])) {?>
				   		<img src="../template/image/<?php echo $news['images_2'];?>" width="100px" height="70px" >
			   		<?php }?>
				</td>
			</tr>
		</tr>
		<tr>
			<td style="font-weight: bold;">Nội dung 2</td>
			<td style="padding: 7px 7px;"><textarea name="txdetail_2" cols="60" rows="13"><?php echo isset($_GET['id']) ? $news['detail_2'] : '';?></textarea></td>
		</tr>
		<tr>
			<td><label>Chuyên mục</label></td>
			<td style="padding: 7px 7px;"><select name="category" style="height: 30px;">
					<option value="0">------Chuyên mục-----</option>
				<?php foreach ($_SESSION['category'] as $value) { ?>
					<option value="<?php echo $value['id_News'];?>">
						<?php echo $value['id_News']." - ".$value['title'];?>
					</option>
				<?php } ?>
			</select></td>
		</tr>
		<tr>
			<td></td>
			<td style="padding: 7px 7px;"><input type="submit" name="btOk_news" value="<?php echo isset($_GET['id']) ? "Sửa" : "Thêm";?>" style="padding:5px 10px; font-weight: bold;"></td>
		</tr>
	</table>
	</form>
</div>