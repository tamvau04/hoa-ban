<?php
$p = isset($_SESSION['product']) ? $_SESSION['product'] : '';
$name = $images = $price = $description = "";

if (isset($_POST['btOk'])) {
	$name = isset($_POST['txname']) && $_POST['txname'] != null ? $_POST['txname'] : '';
	//upload file image
	$file = $_FILES['img'];
	if ($file['name'] == null) {//nếu không chọn file ảnh mới thì sẽ giữ lại file ảnh  cũ
		$images = $p['images'];
	}else{
		if ($file['size'] > 1048576) {
			echo "File không được lớn hơn 1MB";
		}else{
			$images = $file['name'];
			move_uploaded_file($file['tmp_name'], "../template/image/".$images);
		}
	}
	$price = isset($_POST['txprice']) ? $_POST['txprice'] : '';
	$description = $_POST['txdescription'] && $_POST['txdescription'] != null ? $_POST['txdescription'] :'';
	include_once("model/product.php");
	$p = new Product();
	if (isset($_GET['id'])) {
		$p->update($id,$name, $images, $price, $description);
	}else{
		$p->insert($name, $images, $price, $description);
	}
	header("location:index.php?page=product");
}
?>