<?php
include_once("library/config.php");
include_once("library/data.php");

class Products extends Data{

	public function getList(){
		$db = new Data();
		$db->query("SELECT * FROM product");
		return $db->getAll();
	}

	public function getOne($id){
		$db = new Data();
		$db->query("SELECT * FROM product WHERE id_Product = $id");
		return $db->getOneFirst();
	}

	public function getRow(){
		$db = new Data();
		$db->query("SELECT * FROM product");
		return $db->num_row();
	}

	public function getListPage($start, $limit){//lấy ra số bản ghi tối đa trong 1 page bắt đầu từ bản ghi $start
		$db = new Data();
		$db->query("SELECT * FROM `product` LIMIT $start,$limit");
		return $db->getAll();
	}
}
?>