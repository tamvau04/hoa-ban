<?php
$action = isset($_GET['action']) && $_GET['action'] != null ? $_GET['action'] :'';
switch ($action) {
	case 'insert':
		include_once('view/cart/insert.php');
		break;
	case 'delete':
		include_once('view/cart/delete.php');
		break;
	case 'update':
		include_once('view/cart/update.php');
		break;
	default:
		include_once('view/cart/list.php');
		break;
}
?>