<?php 
include_once("library/config.php");
include_once("library/data.php");

class News extends Data{

	public function getList(){
		$db = new Data();
		$db->query("SELECT * FROM news");
		return $db->getAll();
	}

	public function getList_wedding(){
		$db = new Data();
		$db->query("SELECT * FROM article INNER JOIN news ON article.id_News = news.id_News WHERE article.id_News = 1");
		return $db->getAll();
	}

	public function getList_festival(){
		$db = new Data();
		$db->query("SELECT * FROM article INNER JOIN news ON article.id_News = news.id_News WHERE article.id_News = 2");
		return $db->getAll();
	}

	public function getList_morals(){
		$db = new Data();
		$db->query("SELECT * FROM article INNER JOIN news ON article.id_News = news.id_News WHERE article.id_News = 3");
		return $db->getAll();
	}

	public function getOne($id){
		$db = new Data();
		$db->query("SELECT * FROM article WHERE id_Article = $id");
		return $db->getOneFirst();
	}
}
?>