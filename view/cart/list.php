<h2 style="text-align: center; color: red;">Thông tin giỏ hàng</h2>
<?php if (isset($_SESSION['cart']) && $_SESSION['cart'] != null) { ?>
<form action="index.php?page=cart&action=update" method="post">
	<table border="1" width="700px" style="margin-left: 100px; margin-top: 20px;">
		<tr style="background:#7f827b; height: 50px; text-align: center; font-weight: bold;">
			<td>Tên Sản phẩm</td>
			<td>Hình ảnh</td>
			<td>Đơn giá</td>
			<td>Số lượng</td>
			<td>Thành tiền</td>
			<td>Xóa</td>
		</tr>
		<?php foreach ($_SESSION['cart'] as $value) { ?>
			<tr style="text-align: center;">
				<td><?php echo $value['name'];?></td>
				<td><img src="template/image/<?php echo $value['images'];?>" width="120px" height="100px" style="border: 1px solid #7f827b; border-radius:10px;"></p></td>
				<td><?php echo number_format($value['price']);?> đồng</td>
				<td><input type="text" name="<?php echo $value['id_Product'];?>" value="<?php echo $value['quantity'] ?>" style="height: 30px; width:50px; text-align: center;"></td>
				<td><?php echo number_format($value['quantity'] * $value['price']);?> đồng</td>
				<td width="10%"><a href="index.php?page=cart&action=delete&id=<?php echo $value['id_Product'];?>"><img src="template/image/delete.png" width="60px" height="50px"></a></td>
			</tr>
		<?php } ?>
	</table>
	<input type="submit" name = "btUpdate" value="Sửa" style="margin-left: 740px; margin-top: 10px; height: 35px; width: 60px; font-weight: bold; border: 1px solid black; border-radius: 5px;">
</form>
<?php }else{
	include('view/cart/notProduct.php');
}?>

