<?php 
session_start();
ob_start(); 
?>
<!DOCTYPE html>
<html>
<head>
	<title>Melissa Flower</title>
	<meta charset = "utf-8">
	<link rel="stylesheet" type="text/css" href="template/css/style.css">
	<link rel="stylesheet" type="text/css" href="template/css/content.css">
	<link rel="stylesheet" type="text/css" href="template/css/one.css">
	<li
</head>
<body>
<?php if (!isset($_SESSION['user']) && !isset($_SESSION['pass'])) {?>
	<div id="container">
		<div id="header">
			<?php include("template/header.php") ?>
		</div>
		<div class="content">
			<?php
			$pageArray = array('introduce','product','news','map','cart','login','register');
			if (isset($_GET['page'])) {
				$page = $_GET['page'];
				if (in_array($page,$pageArray)) {
					if($page == 'introduce' || $page == 'map'){
						include('view/'.$page.'.php');
					}else{
						include('control/'.$page.'Control.php');
					}	
				}
			}else{
				include('control/homeControl.php');
			}
			?>
		</div>
		<div id="footer">
			<?php include("template/footer.php");?>
		</div>
	</div>
<?php }else{
	 header('location:admin/index.php');
}?>
</body>
</html>