<?php
include_once("../library/config.php");
include_once("../library/data.php");

class News extends Data{

	public function getList(){
		$db = new Data();
		$db->query("SELECT * FROM article INNER JOIN news ON article.id_News = news.id_News");
		return $db->getAll();
	}

	public function getListPage($start, $limit){
		$db = new Data();
		$db->query("SELECT * FROM article INNER JOIN news ON article.id_News = news.id_News LIMIT $start,$limit");
		return $db->getAll();
	}

	public function getListCategory(){
		$db = new Data();
		$db->query("SELECT * FROM news");
		return $db->getAll();
	}

	public function getOne($id){
		$db = new Data();
		$db->query("SELECT * FROM article INNER JOIN news ON article.id_News = news.id_News WHERE id_Article = $id");
		return $db->getOneFirst();
	}

	public function insert($title_article,$description,$images_1,$images_2, $detail_1, $detail_2,$id_News){
		$db = new Data();
		$db->query("INSERT INTO `article`(`title_article`, `description_article`, `images_1`, `images_2`,`detail_1`,`detail_2`,`id_News`) VALUES('$title_article','$description','$images_1','$images_2', '$detail_1', '$detail_2','$id_News')");
	}

	public function update($id_Article,$title_article,$description,$images_1,$images_2, $detail_1, $detail_2,$id_News){
		$db = new data();
		$db->query("UPDATE `article` SET `title_article` = '$title_article', `description_article` = '$description', `images_1` ='$images_1', `images_2`= '$images_2', `detail_1` ='$detail_1', `detail_2` ='$detail_2', `id_News` = '$id_News' WHERE `id_Article` = '$id_Article'");
	}

	public function delete($id){
		$db = new data();
		$db->query("DELETE FROM article WHERE id_Article = $id");
	}
}
?>