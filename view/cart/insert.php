<?php
$idProduct = $_GET['id'];
$newProduct = array();
foreach ($_SESSION['list_Product'] as $value) {
	$newProduct[$value['id_Product']] = $value;
}
if (!isset($_SESSION['cart']) || $_SESSION['cart'] == null) {//chua co san pham nao trong cart
	$newProduct[$idProduct]['quantity'] = 1;
	$_SESSION['cart'][$idProduct] = $newProduct[$idProduct];
} else{//da co san pham trong gio
	if (array_key_exists($idProduct, $_SESSION['cart'])) {//san pham nay da co trong gio
		$_SESSION['cart'][$idProduct]['quantity'] ++;
	}else{//san pham nay chua co trong gio
		$newProduct[$idProduct]['quantity'] = 1;
		$_SESSION['cart'][$idProduct] = $newProduct[$idProduct];
	}
}
header("location:index.php?page=product");
?>