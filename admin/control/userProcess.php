<?php 
$user = isset($_SESSION['userOne']) ? $_SESSION['userOne'] : '';
$username = $password = $fullname = "";

if (isset($_POST['btUser'])) {
	$username = isset($_POST['txusername']) && $_POST['txusername'] != null ? $_POST['txusername'] :'';
	$password = isset($_POST['txpassword']) && $_POST['txpassword'] != null ? $_POST['txpassword'] :'';
	$fullname = isset($_POST['txfullname']) && $_POST['txfullname'] != null ? $_POST['txfullname'] :'';

	$day = isset($_POST['txday']) ? $_POST['txday'] : '';
	$month = isset($_POST['txmonth']) ? $_POST['txmonth'] :'';
	$year = isset($_POST['txyear']) ? $_POST['txyear'] :'';
	$birthday = $year.'-'.$month.'-'.$day;
	// $birthday = date("Y-m-d",strtotime($newDate));

	$sex = isset($_POST['sex']) && $_POST['sex'] == 1 ? 1 : 0;
	if (!isset($_POST['authority']) || $_POST['authority'] == 0) {
		$id_authority  = $user['id_authority'];
	}else{
		$id_authority = $_POST['authority'];
	}

	include_once('model/user.php');
	$u = new User();
	if (isset($_GET['id'])) {
		$u->update($_GET['id'],$username,$password,$fullname,$birthday,$sex,$id_authority);
	}else{
		$u->insert($username,$password,$fullname,$birthday,$sex,$id_authority);
	}
	header("location:index.php?page=user");
}
?>